import React,{useState} from 'react'
import { Container } from 'react-bootstrap'
import {useParams} from 'react-router'
function ViewArticle() {

  const [article, setArticle] = useState()
  const {id} = useParams()


  return (
    <Container>
        {article ? <>
            <h1>{article.title}</h1>
            <p>{article.description}</p>
        </>:
        ''}
    </Container>
  )
}

export default ViewArticle
