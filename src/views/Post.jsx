import React,{useState} from "react";
import { Col, Container, Row,Form,Button } from "react-bootstrap";
import { postArticle, uploadImage } from "../services/article_service";

function Post() {


  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
  const [imageFile, setImageFile] = useState(null)

  const onAdd = async(e)=>{
      e.preventDefault()
      let article = {
          title,description
      }
      if(imageFile){
         let url = await uploadImage(imageFile)
         article.image = url
      }
      postArticle(article).then(message=>alert(message))
  }

  return (
    <Container>
      <h1 className="my-2">New Article</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="title">
              <Form.Label>Title</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Title" 
                value={title}
                onChange={(e)=>setTitle(e.target.value)}
                />
              <Form.Text className="text-muted">
               
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Description" 
                value={description}
                onChange={(e)=>setDescription(e.target.value)}
                />
            </Form.Group>
            <Button 
                variant="primary" 
                type="submit"
                onClick={onAdd}
            >
              Submit
            </Button>
          </Form>
        </Col>
        <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="Choose Image" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Post;
